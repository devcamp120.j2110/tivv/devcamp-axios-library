import 'bootstrap/dist/css/bootstrap.min.css'
import AxiosLibrary from './components/axiosLibrary';


function App() {
  return (
    <div>
      <AxiosLibrary/>
    </div>
  );
}

export default App;
